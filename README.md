[![Maven Central](https://img.shields.io/maven-central/v/io.ultreia.maven/gitlab-maven-plugin.svg)](http://mvnrepository.com/artifact/io.ultreia.maven/gitlab-maven-plugin)

# Gitlab Maven Plugin

Goals:

 * download-milestones
 * generate-changes
 * generate-changelog
 
 Documentation will come soon. Be patient a bit.