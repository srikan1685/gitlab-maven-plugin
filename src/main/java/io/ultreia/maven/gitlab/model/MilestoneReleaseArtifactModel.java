package io.ultreia.maven.gitlab.model;

import org.apache.commons.lang3.StringUtils;

import java.net.URL;

/**
 * Created by tchemit on 20/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class MilestoneReleaseArtifactModel {

    private final String name;
    private final String filename;
    private final URL url;

    public MilestoneReleaseArtifactModel(String name, URL url) {
        this.name = name;
        this.url = url;
        this.filename = StringUtils.substringAfterLast(url.toString(),"/");
    }

    public String getName() {
        return name;
    }

    public URL getUrl() {
        return url;
    }

    public String getFilename() {
        return filename;
    }
}
