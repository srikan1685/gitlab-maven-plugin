package io.ultreia.maven.gitlab.model;

import org.apache.commons.lang3.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by tchemit on 20/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReleaseArtifactModel {

    private String groupId;
    private String artifactId;
    private String type;
    private String classifier;
    private String name;

    public String getGroupId() {
        return groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public String getType() {
        return type;
    }

    public String getClassifier() {
        return classifier;
    }

    public String getName() {
        return name;
    }

    public URL getUrl(String host, String version) {
        try {
            return new URL(String.format("%s/%s/%s/%s/%s%s-%s.%s",
                    host,
                    groupId.replaceAll("\\.", "/"),
                    artifactId,
                    version,
                    artifactId,
                    StringUtils.isEmpty(classifier) ? "" : "-" + classifier,
                    version,
                    type));
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setClassifier(String classifier) {
        this.classifier = classifier;
    }

    public void setName(String name) {
        this.name = name;
    }
}
