# GitLab Maven plugin

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2017-02-21 10:18.

## Version [1.0.4](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/milestones/6)

**Closed at 2017-02-21.**


### Issues
  * [[Feature 11]](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/issues/11) **Improve trackers usage in generate mojo configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 12]](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/issues/12) **Add staging mode for changelog generation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.3](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/milestones/5)

**Closed at 2017-02-20.**


### Issues
  * [[Feature 8]](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/issues/8) **Add deployed artifacts for each release into generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 9]](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/issues/9) **show only closed milestones in generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 10]](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/issues/10) **Improve issues display in generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.2](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/milestones/4)

**Closed at 2017-02-20.**


### Issues
  * [[Bug 5]](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/issues/5) **Fix changelog template (missing a line after each milestone)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 6]](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/issues/6) **Add milestone link in generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 7]](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/issues/7) **Improve date format in generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.1](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/milestones/3)

**Closed at 2017-02-20.**


### Issues
  * [[Bug 4]](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/issues/4) **Fix changelog template (missing a line after each tracker)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.0](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/milestones/1)

**Closed at 2017-02-20.**


### Issues
  * [[Feature 1]](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/issues/1) **Add download-milestones mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 2]](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/issues/2) **Add generate-changes mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 3]](https://gitlab.com/maven.ultreia.io/gitlab-maven-plugin/issues/3) **Add generate-changelog mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

